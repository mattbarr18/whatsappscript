import sys


if len(sys.argv) < 2:
    print("Please specify a file. Use the following format: python whatsApp.py myFile.txt")
    sys.exit()
else:
    file = sys.argv[1]

chat_list = []
with open(file, encoding="utf-8") as f:

    for line in f:
        if line[0].isdigit() and '/' in line[0:3]:
            if line.count(':') < 2:
                continue
            line_list = []
            date = line[0:line.find(',')]
            line_list.append(date)
            time = line[(line.find(',') + 2): (line.find('-') - 1)]
            line_list.append(time)
            sender = line[(line.find('-') + 2): line.find(": ")]
            line_list.append(sender)
            text = line[(line.find(": ") + 2): -1]
            if text[0] == '<':
                text = ''
            line_list.append(text)
            chat_list.append(line_list)
        else:
            chat_list[-1][3] = (chat_list[-1][3] + line).replace("\n", "")

dates = dict()
times = dict()
names = dict()
words = dict()

for chat in chat_list:
    if chat[0] not in dates:
        dates[chat[0]] = 1
    else:
        dates[chat[0]] += 1

    if (chat[1][0:chat[1].find(':')] + chat[1][-2:]) not in times:
        times[(chat[1][0:chat[1].find(':')] + chat[1][-2:])] = 1
    else:
        times[(chat[1][0:chat[1].find(':')] + chat[1][-2:])] += 1

    if chat[2] not in names:
        names[chat[2]] = 1
    else:
        names[chat[2]] += 1

    chat_words = chat[3].split()
    for word in chat_words:
        word = word.lower()
        if len(word) > 0:
            while not word[0].isalpha():
                word = word[1:]
                if len(word) == 0:
                    break
        if len(word) > 0:
            while not word[-1].isalpha():
                word = word[:-1]
                if len(word) == 0:
                    break
        if len(word) == 0:
            break
        if word not in words:
            words[word] = 1
        else:
            words[word] += 1

print("Top 20 dates:")
rank = 1
dates_rank = sorted(list(dates.items()), key=lambda x: int(x[1]), reverse=True)
for row in dates_rank:
    if rank > 20:
        break
    print(str(rank) + ") " + row[0] + ": " + str(row[1]) + " messages")
    rank += 1


print("\n\nTop hours of the day:")
rank = 1
times_rank = sorted(list(times.items()), key=lambda x: int(x[1]), reverse=True)
for row in times_rank:
    print(str(rank) + ") " + row[0] + ": " + str(row[1]) + " messages")
    rank += 1

print("\n\nTop texters:")
rank = 1
names_rank = sorted(list(names.items()), key=lambda x: int(x[1]), reverse=True)
for row in names_rank:
    print(str(rank) + ") " + row[0] + ": " + str(row[1]) + " messages")
    rank += 1

print("\n\nTop 100 words:")
rank = 1
words_rank = sorted(list(words.items()), key=lambda x: int(x[1]), reverse=True)
for row in words_rank:
    if rank > 100:
        break
    print(str(rank) + ") " + row[0] + ": " + str(row[1]) + " occurrences")
    rank += 1
